const axios = require('axios')
const puppeteer = require('puppeteer')
const { id, pass } = require('./connect')
const url = 'https://www.viadeo.com'
const api = process.env.NODE_ENV === 'prod' ? 'http://192.168.99.100:4000' : 'http://127.0.0.1:4000'
let profiles = []
let page = []
let browser

class Crawler {
  async accessSearchPage () {
    this.log('Waiting /GET search page...')
    const form = await page[0].$('.search-form.core-form')
    await form.evaluate(form => form.submit())
    await this.catchProfiles()
  }

  async addProfiles (contacts) {
    let i = 0
    this.log(`${contacts.length} contacts founded`)
    contacts.forEach(idProfile => {
      if (profiles.indexOf(idProfile) == -1) {
        i++
        profiles.push(idProfile)
        axios({
          url: `${api}/links/create`,
          method: 'POST',
          data: { idProfile }
        }).catch(e => console.log(`axios ${e} id : ${idProfile}`))
      }
    })
    this.log(`Pushing ${i} new users to the db`)
    this.log(`New user base length : ${profiles.length}`)
  }

  async catchProfiles () {
    page[0].on('response', async response => {
      if (/^https:\/\/www\.viadeo\.com\/vws\/search\/searchResults\?q=/.test(response.url())) {
        this.log('Catching profile links...')
        const items = await response.json()
        this.addProfiles(items.profiles.map(profile => profile.profileUrl))
        if (profiles.length < 20) {
          this.log('/GET next page')
          this.nextPage()
        } else {
          await page[0].removeAllListeners('response')
          this.getUsers()
        }
      }
    })
  }

  async connectViadeo () {
    this.log('Starting browser')
    browser = await puppeteer.launch({args: [
      '--no-sandbox',
      '--disable-setuid-sandbox'
    ]})
    page[0] = await browser.newPage()
    page[0].setDefaultNavigationTimeout(0)
    this.log('Waiting /GET https://secure.viadeo.com/fr/signin...')
    await page[0].goto('https://secure.viadeo.com/fr/signin')
    await page[0].type('input[name="email"]', id)
    await page[0].type('input[name="password"]', pass)
    this.log('Connecting to Viadeo')
    await page[0].click('button[type="submit"]')
    this.log('Waiting connexion status...')
    await page[0].waitForNavigation()
    this.log('Connexion success')
    if (profiles.length === 0) {
      this.accessSearchPage()
    } else {
      this.getUsers()
    }
  }

  async getContacts (idProfile = '', thePage) {
    await thePage.goto(idProfile, {"waitUntil":["load", "networkidle0"]})
    await thePage.evaluate(async () => {
      let i = 18
      await new Promise(resolve => {
        let interval = setInterval(() => {
          if (document.querySelector(`.contact-list:nth-child(${i})`) === null) {
            resolve(clearInterval(interval))
          }
          document.querySelector(`.contact-list:nth-child(${i})`).scrollIntoView()
          i += 17
        }, 1500)
      })
    })
    const newContacts = await thePage.evaluate(() =>
      [...document.querySelectorAll('div > div.bd > span > a')].map(link =>
        link.href.split('.com')[1] + '?consultationType=29'
      )
    )
    axios({
      url: `${api}/links/update`,
      method: 'PUT',
      data: { contacts: true, idProfile: `${idProfile.split('.com')[1].split('/contacts')[0].split('?')[0]}?consultationType=29` }
    })
    this.addProfiles(newContacts)
  }

  async getProfilesFromDb () {
    return (await axios(`${api}/links/show?contacts=false`)).data.map(profiles => profiles.idProfile)
  }

  async getUsers () {
    let i = 0
    let promises = []
    await page[0].close()
    while (i < 6000000) {
      for (let j = 0; j <= 10; j++) {
        page[j] = await browser.newPage()
        page[j].setDefaultNavigationTimeout(0)
        const link = `${url}${profiles[i].split('?')[0]}/contacts`
        this.log(`Waiting user profile ${profiles[i]}...`)
        promises.push(this.getContacts(link, page[j]))
        i++
      }
      await Promise.all(promises)
      for (let j = 0; j < page.length; j++) {
        await page[j].close()
      }
      promises = []
    }
  }

  log (string) {
    console.log(string)
    console.log('=====================================')
  }

  async nextPage () {
    await page[0].$('ul.paginator li.page:not(.disabled) a')
    await page[0].click('ul.paginator li.page:not(.disabled) a')
  }

  async run () {
    this.log('Crawler viadeo is starting')
    this.log('Pulling profiles from db')
    profiles = await this.getProfilesFromDb()
    this.connectViadeo()
  }
}

const app = new Crawler()
app.run()
