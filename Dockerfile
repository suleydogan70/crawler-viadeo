FROM alekzonder/puppeteer:latest

WORKDIR /app

COPY . /app

RUN npm i

EXPOSE 9000

CMD [ "npm", "run", "prod" ]
