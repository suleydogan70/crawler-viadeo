const puppeteer = require('puppeteer')
const axios = require('axios')
const { id, pass } = require('./connect')
const api = process.env.NODE_ENV === 'prod' ? 'http://192.168.99.100:4000' : 'http://127.0.0.1:4000'
const url = 'https://www.viadeo.com'
let profiles = []
let page = []
let browser

class Crawler {
  async getProfilesFromDb () {
    return (await axios(`${api}/links/show?state=false`)).data.map(profiles => profiles.idProfile)
  }

  async getContacts (thePage) {
    return await thePage.evaluate(() =>
      [...document.querySelectorAll('div > div.bd > span > a')].map(link =>
        link.href.split('.com')[1] + '?consultationType=29'
      )
    )
  }

  async connectViadeo () {
    this.log('Starting browser')
    browser = await puppeteer.launch({args: [
      '--no-sandbox',
      '--disable-setuid-sandbox'
    ]})
    page[0] = await browser.newPage()
    page[0].setDefaultNavigationTimeout(0)
    this.log('Waiting /GET https://secure.viadeo.com/fr/signin...')
    await page[0].goto('https://secure.viadeo.com/fr/signin')
    await page[0].type('input[name="email"]', id)
    await page[0].type('input[name="password"]', pass)
    this.log('Connecting to Viadeo')
    await page[0].click('button[type="submit"]')
    this.log('Waiting connexion status...')
    await page[0].waitForNavigation()
    this.log('Connexion success')
    this.crawl()
  }

  async crawl () {
    for (let i = 0; i < profiles.length;) {
      let promises = []
      for (let j = 0; j <= 20; j++) {
        page[j] = await browser.newPage()
        page[j].setDefaultNavigationTimeout(0)
        promises.push(this.getUserData(`${url}${profiles[i]}`, page[j]))
        i++
      }
      await Promise.all(promises)
      promises = []
      for (let j = 0; j < page.length; j++) {
        await page[j].close()
      }
    }
  }

  async getUserData(idProfile, thePage) {
    let user = {}
    await thePage.goto(idProfile, { waitUntil: 'networkidle0' })
    const fullname = await thePage.$eval('h1', h1 => h1.innerText.trim().split(' '))
    this.log(`Crawling user ${fullname[0]} ${fullname[1]} data`)
    user.idProfile = idProfile
    user.firstname = fullname[0]
    user.lastname = fullname[1]
    const userPlus = await thePage.evaluate(async thePage => {
      const json = {
        description: document.querySelector('p.profile-headline').innerText.trim(),
        avatar: document.querySelector('.main-avatar-container .avatar img.profile-avatar-img').getAttribute('src'),
        career: [],
        skills: [],
        languages: [],
        interests: [],
        contacts: []
      }
      await new Promise((resolve, reject) => {
        const distance = 100
        let totalHeight = 0
        const timer = setInterval(() => {
          const scrollHeight = document.body.scrollHeight
          window.scrollBy(0, distance)
          totalHeight += distance

          if (totalHeight >= scrollHeight) {
            clearInterval(timer)
            setTimeout(resolve, 1000)
          }
        }, 100)
      })
      const careers = [...document.querySelectorAll('article header h1 a')]
      const skills = [...document.querySelectorAll('.profile-skills .profile-skill h3')]
      const languages = [...document.querySelectorAll('.profile-languages h3')]
      const interests = [...document.querySelectorAll('.profile-hobbies-item span')]
      careers.forEach(career => {
        json.career.push(career.innerText.trim())
      })
      skills.forEach(skill => {
        json.skills.push(skill.innerText.trim())
      })
      languages.forEach(lang => {
        json.languages.push(lang.innerText.trim())
      })
      interests.forEach(hobbie => {
        json.interests.push(hobbie.innerText.trim())
      })
      return await json
    })
    if (await thePage.$('.profile-card__contacts__count a')) {
      await thePage.click('.profile-card__contacts__count a')
      await thePage.waitForSelector('.popin-container .contact-list')
      const contacts = await this.getContacts(thePage)
      user = {...user, ...userPlus}
      user.contacts = contacts
    } else {
      user = {...user, ...userPlus}
    }
    axios({
      url: `${api}/user/create`,
      method: 'POST',
      data: user
    }).catch(e => {
      console.log('post user')
      console.log(e)
    })
    axios({
      url: `${api}/links/update`,
      method: 'PUT',
      data: { state: true, idProfile: user.id }
    }).catch(e => {
      console.log('post state')
      console.log(e)
    })
  }

  log (string) {
    console.log(string)
    console.log('=====================================')
  }

  async run () {
    this.log('Crawler viadeo is starting')
    this.log('Pulling profiles from db')
    profiles = await this.getProfilesFromDb()
    this.connectViadeo()
  }
}

const app = new Crawler()
app.run()
